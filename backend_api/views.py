from django.shortcuts import render
import requests


def photos_view(request, items_to_fetch=50, items_to_diplay=10, page_number=17):
    """
    View to fetch N number of photos from https://jsonplaceholder.typicode.com/photos
    Retrieve the album title for those photos from 'https://jsonplaceholder.typicode.com/albums'
    Returns JSON object to the view
    API documentation - https://github.com/typicode/json-server

    :param request:
    :param items_to_fetch:
    :param items_to_diplay:
    :param page_number:
    :return:
    """
    url_photos = 'https://jsonplaceholder.typicode.com/photos'
    url_albums = 'https://jsonplaceholder.typicode.com/albums'

    try:
        # Request photos use pagination and limits to select 50 items  ?_page=1&_limit=50 paginate
        photos_request = requests.get(url_photos+'?_page='+str(page_number)+'_limit='+str(items_to_fetch))
        first_ten_photos = photos_request.json()[0:items_to_diplay]

        # Select unique albumIds from first ten photos and create a query string for next url
        album_ids = set([album['albumId'] for album in first_ten_photos])
        query_string = ''.join(map(lambda x: 'id=' + str(x) + '&', album_ids))

        # fetch those albums and append titles to
        url_query_string = url_albums + '?' + query_string
        second_request = requests.get(url_query_string)
        albums = second_request.json()

        # Dictionary to map album titles to ids
        album_titles_dictionary = {}
        for album in albums:
            album_titles_dictionary[album['id']] = album['title']

        # Loop through the photos and use dictionary above to append album title to each photo
        for photo in first_ten_photos:
            photo['albumTitle'] = album_titles_dictionary[photo['albumId']]

        data = first_ten_photos

        context = {
            'data': data
        }

        print(second_request.json())
        return render(request, 'index.html', context)

    except Exception as e:
        # TODO add use requests lib exception classes
        instance = str(type(e))
        message = str(e)
        context = {
            'typeoferror': instance,
            'error message': message
        }
        return render(request,  'index.html', context )