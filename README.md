# API Calls app #

## Test two - API Calls

### Backend

- Django backend makes api calls to https://jsonplaceholder.typicode.com using the python requests lib
- API documentation for source is at https://github.com/typicode/json-server
- Data is rendered to the view in the context dictionary as a data json object

### Frontend

- Backend renders html page
- Data for the table is provided to the Stenciljs table component via 'data' variable
 

